#ifndef SIMCON_MESSAGE_H
#define SIMCON_MESSAGE_H

#include <windows.h>
#include <SimConnect.h>
#include <memory>

#define CAST_TO(type, name) \
    std::shared_ptr<const type> name() const {\
       return std::static_pointer_cast<const type>(data);\
    };

class Message {
public:
    explicit Message(SIMCONNECT_RECV *data) : data(std::shared_ptr<const SIMCONNECT_RECV>(copy_data(data))) {};

    CAST_TO(SIMCONNECT_RECV, as_base);
    CAST_TO(SIMCONNECT_RECV, as_is);
    CAST_TO(SIMCONNECT_RECV_EXCEPTION, as_exception);
    CAST_TO(SIMCONNECT_RECV_OPEN, as_open);
    CAST_TO(SIMCONNECT_RECV_EVENT, as_event);
    CAST_TO(SIMCONNECT_RECV_SIMOBJECT_DATA, as_simobject_data);

protected:
    const std::shared_ptr<const SIMCONNECT_RECV> data;

    static SIMCONNECT_RECV* copy_data(SIMCONNECT_RECV *data) {
        SIMCONNECT_RECV *data_copy = nullptr;
        if (data != nullptr) {
            data_copy = (SIMCONNECT_RECV*)malloc(data->dwSize);
            memcpy(data_copy, data, data->dwSize);
        }
        return data_copy;
    }
};


#endif //SIMCON_MESSAGE_H

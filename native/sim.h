#ifndef SIMCON_SIM_H
#define SIMCON_SIM_H

#include <exception>
#include <cstdio>
#include <string>
#include <vector>
#include <thread>
#include <windows.h>
#include <SimConnect.h>
#include <Python.h>

#include "message.h"

class SimException : public std::exception {
public:
    HRESULT hresult;
    explicit SimException(HRESULT hresult) : hresult(hresult) {};
};

enum DataRequestFlag {
    DEFAULT = SIMCONNECT_DATA_REQUEST_FLAG_DEFAULT,
    CHANGED = SIMCONNECT_DATA_REQUEST_FLAG_CHANGED,
    TAGGED = SIMCONNECT_DATA_REQUEST_FLAG_TAGGED,
};

class Sim {
public:
    explicit Sim(const std::string &app_name);

    void add_to_data_definition(SIMCONNECT_DATA_DEFINITION_ID definition_id, const char *var, const char *units, SIMCONNECT_DATATYPE datum_type) const;
    void request_data_on_sim_object_type(DWORD request_id, DWORD definition_id) const;
    void request_data_on_sim_object(DWORD request_id, DWORD definition_id, SIMCONNECT_OBJECT_ID object_type, SIMCONNECT_PERIOD period, SIMCONNECT_DATA_REQUEST_FLAG flags, DWORD origin, DWORD interval, DWORD limit) const;
    void subscribe_to_system_event(SIMCONNECT_CLIENT_EVENT_ID event_id, const char *event_name) const;
    void unsubscribe_from_system_event(SIMCONNECT_CLIENT_EVENT_ID event_id) const;
    DWORD get_last_sent_packet_id() const;
    Message receive() const;
    void interrupt_receive() const;

    ~Sim();

protected:
    HANDLE hSimConnect{};
    HANDLE hEventHandle{};
};

#endif //SIMCON_SIM_H